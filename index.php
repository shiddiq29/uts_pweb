<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Biodata Mahasiswa</title>
  <link rel="stylesheet" href="style.css" />

</head>

<body>
  <script src="java.js"></script>
  
  <div class="center">

    <h1>DATA MAHASISWA</h1>
    <br><br>

    <form name='biodata' method='post' action='tutor.html'>

      <br>
      NPM : <input type='number' name='nim'>
      <p id='val_nim'> </p>
      Nama : <input type='text' name='nama'>
      <p id='val_nama'></p>
      Alamat : <input type="text" name="alamat">
      <p id="val_alamat"></p>
      Agama : <select name='agama'>
        <option>Silahkan Pilih..
        <option>Islam
        <option>Hindu
        <option>Budha
        <option>Kristen
        <option>Konghucu
      </select>
      <p>*Wajib lengkapi data diatas!</p>


      <input type='button' onClick='terimainput()' value='Simpan'>

      <input type='reset' value='Hapus'>


    </form>
  </div>

  <br>

  <div id="table">
    <table class="table"></table>
    <table border='2' id='tabelinput'>
      <tr>
        <td>NPM</td>
        <td>NAMA</td>
        <td>ALAMAT</td>
        <td>AGAMA</td>
      </tr>
    </table>
  </div>


</body>

</html>